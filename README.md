# Freeswitch database authentication module 
 mod_db_auth allow you to authenticate users using SQL queries to external database.
```
## Installation
```sh
$ mkdir -p /usr/src && cd /usr/src/
$ git clone git@gitlab.com:ceco_p/mod_dbauth.git
# $(PWD_FREESWITCH_SRC) - path to freeswitch source files
$ cp -a ./mod_dbauth/mod_db_auth $(PWD_FREESWITCH_SRC)/src/mod/directories/
$ cd $(PWD_FREESWITCH_SRC)
# Add to modules.conf parameter for build mod_apn
echo 'directories/mod_db_auth' >> modules.conf
# Add to configure.ac configuration for create Makefile for mod_apn (AC_CONFIG_FILES array section)
$ sed -i '/src\/mod\/endpoints\/mod_sofia\/Makefile/a src\/mod\/directories\/mod_db_auth\/Makefile' configure.ac
$ ./configure
$ make
$ make install
```
## Configuration
Change db_auth.conf.xml with your sql query and map database columns
Copy configuration file into freeswitch configuration folder.
```sh
$ cp /usr/src/mod_dbauth/conf/db_auth.conf.xml /etc/freeswitch/autoload_configs/
```
**column_mapping** section:
Maps user variable name to column name (as value attribute)

### Module configuration example
```xml
<configuration name="db_auth.conf" description="Database based authentication">
 <settings>

	<param name="odbc-dsn" value="pgsql://hostaddr=$${odbc_host} dbname=$${odbc_db} user=$${odbc_user} password=$${odbc_pass} options='-c client_min_messages=NOTICE'" />    
	<param name="dial-string" value="^:sip_invite_domain=${dialed_domain}:presence_id=${dialed_user}@${dialed_domain}}${sofia_contact(*/${dialed_user}@${dialed_domain})},${verto_contact(${dialed_user}@${dialed_domain})}" />	 
	<param name="sql-tmpl" value="select user_id, domain, password, number_alias, cidr, toll_allow, user_context, default_gateway, 
		effective_caller_id_name, effective_caller_id_number, outbound_caller_id_name, outbound_caller_id_number, 
		call_group, mailbox from phone_users where user_id='${user}' and  domain = '${domain}' and active = true limit 1" />
	
	<column_mapping>
		<param name="user_id" value="user_id"/>
		<param name="domain" value="domain"/>
		<param name="password" value="password"/>
		<param name="number_alias" value="number_alias"/>
		<param name="cidr" value="cidr"/>
		<param name="toll_allow" value="toll_allow"/>
		<param name="user_context" value="user_context"/>
		<param name="default_gateway" value="default_gateway"/>
		<param name="effective_caller_id_name" value="effective_caller_id_name"/>
		<param name="effective_caller_id_number" value="effective_caller_id_number"/>
		<param name="outbound_caller_id_name" value="outbound_caller_id_name"/>
		<param name="outbound_caller_id_number" value="outbound_caller_id_number"/>
		<param name="call_group" value="call_group"/>
		<param name="mailbox" value="mailbox"/>
	</column_mapping>
  </settings>
</configuration>
```

#### SQL Template variables
Support all REQUEST_PARAMS event header variables.
Used in example:
${user}
${domain}

https://freeswitch.org/confluence/display/FREESWITCH/Events
 
## Auto load
```sh
$ sed -i '/<load module="mod_sofia"\/>/a <load module="mod_db_auth"\/>' /ect/freeswitch/modules.conf.xml
```
## Manual load
```sh
$ fs_cli -rx 'load mod_db_auth'
```