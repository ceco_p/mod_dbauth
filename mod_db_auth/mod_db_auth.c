#include <switch.h>

#define CONFIG_FILE "db_auth.conf"

SWITCH_MODULE_LOAD_FUNCTION(mod_db_auth_load);
SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_db_auth_shutdown);
SWITCH_MODULE_DEFINITION(mod_db_auth, mod_db_auth_load, mod_db_auth_shutdown, NULL);

static struct {
	switch_memory_pool_t *pool;
	switch_sql_queue_manager_t *qm;
	switch_mutex_t *dbh_mutex;
} globals;

struct column_mapping {
	char *user_id;
	char *domain;
	char *password;
	char *number_alias;
	char *cidr;
	char *toll_allow;
	char *user_context;
	char *default_gateway;
	char *effective_caller_id_name;
	char *effective_caller_id_number;
	char *outbound_caller_id_name;
	char *outbound_caller_id_number;
	char *call_group;
	char *mailbox;
};
typedef struct column_mapping column_mapping_t;

struct xml_binding {
	char *odbc_dsn;
	char *dial_string;
	char *sql_tmpl;
	column_mapping_t column_mapping;
};
typedef struct xml_binding xml_binding_t;

struct user_data {
	char *user_id;
	char *domain;
	char *password;
	char *number_alias;
	char *cidr;
	char *toll_allow;
	char *user_context;
	char *default_gateway;
	char *effective_caller_id_name;
	char *effective_caller_id_number;
	char *outbound_caller_id_name;
	char *outbound_caller_id_number;
	char *call_group;
	char *mailbox;
	xml_binding_t *bindings;
	switch_bool_t found;
	switch_memory_pool_t *pool;
};
typedef struct user_data user_data_t;

static switch_cache_db_handle_t *get_db_handle(char *odbc_dsn)
{
	switch_cache_db_handle_t *dbh = NULL;
	if (switch_cache_db_get_db_handle_dsn(&dbh, odbc_dsn) != SWITCH_STATUS_SUCCESS) { dbh = NULL; }
	return dbh;
}

static int sql2str_callback(void *pArg, int argc, char **argv, char **columnNames) 
{
	user_data_t *user_data = (user_data_t *)pArg;

	if (argc != 14) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Wrong number of columns %d expect %d\n", argc, 14);
	}
	user_data->user_id = switch_core_strdup(user_data->pool, argv[0]);
	user_data->domain = switch_core_strdup(user_data->pool, argv[1]);
	user_data->password = switch_core_strdup(user_data->pool, argv[2]);
	user_data->number_alias = switch_core_strdup(user_data->pool, argv[3]);
	user_data->cidr = switch_core_strdup(user_data->pool, argv[4]);
	user_data->toll_allow = switch_core_strdup(user_data->pool, argv[5]);
	user_data->user_context = switch_core_strdup(user_data->pool, argv[6]);
	user_data->default_gateway = switch_core_strdup(user_data->pool, argv[7]);
	user_data->effective_caller_id_name = switch_core_strdup(user_data->pool, argv[8]);
	user_data->effective_caller_id_number = switch_core_strdup(user_data->pool, argv[9]);
	user_data->outbound_caller_id_name = switch_core_strdup(user_data->pool, argv[10]);
	user_data->outbound_caller_id_number = switch_core_strdup(user_data->pool, argv[11]);
	user_data->call_group = switch_core_strdup(user_data->pool, argv[12]);
	user_data->mailbox = switch_core_strdup(user_data->pool, argv[13]);

	user_data->found = SWITCH_TRUE;
	return 0; 
}


static switch_bool_t execute_sql_callback(char *odbc_dsn, char *sql, switch_core_db_callback_func_t callback,
										  void *pdata)
{
	switch_bool_t ret = SWITCH_FALSE;
	char *errmsg = NULL;
	switch_cache_db_handle_t *dbh = NULL;

	if (!(dbh = get_db_handle(odbc_dsn))) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_WARNING, "Error Opening DB\n");
		goto end;
	}

	switch_cache_db_execute_sql_callback(dbh, sql, callback, pdata, &errmsg);

	if (errmsg) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_WARNING, "SQL ERR: [%s] %s\n", sql, errmsg);
		free(errmsg);
	}

end:
	if (dbh) { switch_cache_db_release_db_handle(&dbh); }

	return ret;
}

static void db_get_user(char *odbc_dsn, switch_event_t *params, user_data_t *data)
{
	char *query = switch_event_expand_headers(params, data->bindings->sql_tmpl);
	execute_sql_callback(odbc_dsn, query, sql2str_callback, (void *)data);

	if (!zstr(query)) { 
		switch_safe_free(query); 
	}
}

static switch_xml_t create_name_value_node(switch_xml_t parent, const char *node_name, const char *name,
										   const char *value)
{
	switch_xml_t node = switch_xml_add_child(parent, node_name, 0);
	switch_xml_set_attr(node, "name", name);
	switch_xml_set_attr(node, "value", value);

	return node;
}

static switch_xml_t xml_db_fetch(const char *section, const char *tag_name, const char *key_name, const char *key_value,
								 switch_event_t *params, void *user_data)
{
 	if (strcasecmp(section, "directory") != 0) { 
		return NULL;
	}

	switch_event_header_t *even_name_header = switch_event_get_header_ptr(params, "Event-Name");
	if (!even_name_header || strcasecmp(even_name_header->value, "REQUEST_PARAMS") != 0) { 
		return NULL;
	}
 
	switch_event_header_t *key_header = switch_event_get_header_ptr(params, "key");
	if (!key_header || strcasecmp(key_header->value, "id") != 0) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_WARNING, "Expecting header key: id \n");
		return NULL;
	}
	
	switch_memory_pool_t *pool = NULL;
	switch_xml_t xml = NULL;
	switch_core_new_memory_pool(&pool);

	if (!pool) { 
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Unable to allocate memory pool\n");
		return NULL;
	}

	xml_binding_t *bindings = (xml_binding_t*)user_data;

	user_data_t data;
	memset(&data, 0, sizeof(data));
	data.found = SWITCH_FALSE;
	data.pool = pool;
	data.bindings = bindings;
	db_get_user(bindings->odbc_dsn, params, &data);
	
	if (data.found == SWITCH_FALSE) { 
		goto done;
 	}

	xml = switch_xml_new("document");
	switch_xml_t section_node = switch_xml_add_child(xml, "section", 0);
	switch_xml_set_attr(section_node, "name", "directory");

	switch_xml_t domain_node = switch_xml_add_child(section_node, "domain", 0);
	switch_xml_set_attr(domain_node, "name", data.domain);

	switch_xml_t params_node = switch_xml_add_child(domain_node, "params", 0);
	create_name_value_node(params_node, "param", "dial-string", bindings->dial_string);

	switch_xml_t user_node = switch_xml_add_child(domain_node, "user", 0);
	switch_xml_set_attr(user_node, "id", data.user_id);
	switch_xml_set_attr(user_node, "mailbox", data.mailbox);
	switch_xml_set_attr(user_node, "cidr", data.cidr);
 
	switch_xml_t user_params_node = switch_xml_add_child(user_node, "params", 0);	
	create_name_value_node(user_params_node, "param", "password", data.password);
	create_name_value_node(user_params_node, "param", "vm-password", "1000");

	switch_xml_t variables_node = switch_xml_add_child(user_node, "variables", 0);
	create_name_value_node(variables_node, "variable", "user_context", data.user_context);
	create_name_value_node(variables_node, "variable", "toll_allow", data.toll_allow);
	create_name_value_node(variables_node, "variable", "accountcode", "default");
	create_name_value_node(variables_node, "variable", "effective_caller_id_name", data.effective_caller_id_name);
	create_name_value_node(variables_node, "variable", "effective_caller_id_number", data.effective_caller_id_number);
	create_name_value_node(variables_node, "variable", "outbound_caller_id_name", data.outbound_caller_id_name);
	create_name_value_node(variables_node, "variable", "outbound_caller_id_number", data.outbound_caller_id_number);
	create_name_value_node(variables_node, "variable", "callgroup", data.call_group);
  
done:
	if (pool) { 
		switch_core_destroy_memory_pool(&pool); 
	}

	//char *html = switch_xml_toxml(xml, SWITCH_TRUE);

	return xml;
}

static switch_status_t parce_column_mapping(switch_xml_t xml_column_mapping, column_mapping_t *column_mapping)
{
	switch_xml_t param;
	
	for (param = switch_xml_child(xml_column_mapping, "param"); param; param = param->next) {
		char *var = (char *)switch_xml_attr_soft(param, "name");
		char *val = (char *)switch_xml_attr_soft(param, "value");

		if (!strcasecmp(var, "user_id")) {
			column_mapping->user_id = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "domain")) {
			column_mapping->domain = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "password")) {
			column_mapping->password = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "number_alias")) {
			column_mapping->number_alias = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "cidr")) {
			column_mapping->cidr = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "toll_allow")) {
			column_mapping->toll_allow = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "user_context")) {
			column_mapping->user_context = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "default_gateway")) {
			column_mapping->default_gateway = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "effective_caller_id_name")) {
			column_mapping->effective_caller_id_name = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "effective_caller_id_number")) {
			column_mapping->effective_caller_id_number = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "outbound_caller_id_name")) {
			column_mapping->outbound_caller_id_name = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "outbound_caller_id_number")) {
			column_mapping->outbound_caller_id_number = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "call_group")) {
			column_mapping->call_group = switch_core_strdup(globals.pool, val);
		} else if (!strcasecmp(var, "mailbox")) {
			column_mapping->mailbox = switch_core_strdup(globals.pool, val);
		}
	}

	if (!column_mapping->user_id || !column_mapping->domain || !column_mapping->password ||
		!column_mapping->number_alias || !column_mapping->cidr || !column_mapping->toll_allow ||
		!column_mapping->user_context || !column_mapping->default_gateway ||
		!column_mapping->effective_caller_id_name || !column_mapping->effective_caller_id_number ||
		!column_mapping->outbound_caller_id_name || !column_mapping->outbound_caller_id_number ||
		!column_mapping->call_group || !column_mapping->mailbox) {

		return SWITCH_STATUS_NOTFOUND;
	}

	return SWITCH_STATUS_SUCCESS;
}

static switch_status_t do_config(switch_memory_pool_t *pool)
{
	xml_binding_t *bindings;
	switch_xml_t cfg, xml, settings, param, column_mapping;
	char *odbc_dsn = NULL;
	char *dial_string = NULL;
	char *sql_tmpl = NULL;
	switch_status_t status = SWITCH_STATUS_SUCCESS;
	switch_cache_db_handle_t *dbh = NULL;
	globals.pool = pool;

	switch_mutex_init(&globals.dbh_mutex, SWITCH_MUTEX_NESTED, pool);

	if (!(xml = switch_xml_open_cfg(CONFIG_FILE, &cfg, NULL))) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_WARNING, "Open of %s failed\n", CONFIG_FILE);
		return SWITCH_STATUS_TERM;
	}

	if (!(settings = switch_xml_child(cfg, "settings"))) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Invalid configuration\n");
		switch_goto_status(SWITCH_STATUS_FALSE, done);
	}

	for (param = switch_xml_child(settings, "param"); param; param = param->next) {
		char *var = (char *)switch_xml_attr_soft(param, "name");
		char *val = (char *)switch_xml_attr_soft(param, "value");

		if (!strcasecmp(var, "odbc-dsn")) {
			odbc_dsn = val;
		} else if (!strcasecmp(var, "dial-string")) {
			dial_string = val;
		} else if (!strcasecmp(var, "sql-tmpl")) {
			sql_tmpl = val;
		}
	}

	if (!odbc_dsn) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Missing odbc_dsn option.\n");
		switch_goto_status(SWITCH_STATUS_FALSE, done);
	}

	if (!dial_string) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Missing dial_string option.\n");
		switch_goto_status(SWITCH_STATUS_FALSE, done);
	}

	if (!sql_tmpl) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Missing sql-tmpl option.\n");
		switch_goto_status(SWITCH_STATUS_FALSE, done);
	}

	if ((bindings = switch_core_alloc(globals.pool, sizeof(*bindings)))) {
		memset(bindings, 0, sizeof(*bindings));

		bindings->dial_string = switch_core_strdup(globals.pool, dial_string);
		bindings->odbc_dsn = switch_core_strdup(globals.pool, odbc_dsn);
		bindings->sql_tmpl = switch_core_strdup(globals.pool, sql_tmpl);
	}

	if (!(column_mapping = switch_xml_child(settings, "column_mapping"))) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Invalid configuration, missing column mapping\n");
		switch_goto_status(SWITCH_STATUS_FALSE, done);
	}

	if (parce_column_mapping(column_mapping, &bindings->column_mapping) != SWITCH_STATUS_SUCCESS) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Invalid configuration, missing column mapping\n");
		switch_goto_status(SWITCH_STATUS_FALSE, done);
	}

	dbh = get_db_handle(bindings->odbc_dsn);

	if (!dbh) {
		switch_log_printf(SWITCH_CHANNEL_LOG, SWITCH_LOG_ERROR, "Unable to connect to database dbh is NULL.\n");
		switch_goto_status(SWITCH_STATUS_FALSE, done);
	}

	switch_xml_bind_search_function(xml_db_fetch, switch_xml_parse_section_string("directory"), bindings);
	switch_sql_queue_manager_init_name("mod_db_auth", &globals.qm, 2, bindings->odbc_dsn,
									   SWITCH_MAX_TRANS, NULL, NULL, NULL, NULL);
	switch_sql_queue_manager_start(globals.qm);
	
done:

	if (dbh) { switch_cache_db_release_db_handle(&dbh); }

	switch_xml_free(xml);
	return status;
}


SWITCH_MODULE_LOAD_FUNCTION(mod_db_auth_load)
{
	*module_interface = switch_loadable_module_create_module_interface(pool, modname);

	if (do_config(pool) != SWITCH_STATUS_SUCCESS) {
		return SWITCH_STATUS_FALSE; 
	}
	return SWITCH_STATUS_SUCCESS;
}

SWITCH_MODULE_SHUTDOWN_FUNCTION(mod_db_auth_shutdown)
{

	switch_sql_queue_manager_stop(globals.qm);

	if (globals.qm) { 
		switch_sql_queue_manager_destroy(&globals.qm);
	}

	switch_xml_unbind_search_function_ptr(xml_db_fetch);
	return SWITCH_STATUS_SUCCESS;
}
